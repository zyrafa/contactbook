﻿using System;
using System.Xml.Linq;

namespace ContactBook
{
    public class XmlMapper : IContentMapper<ContactBook>
    {
        private const string ContactElementName = "contact";
        private const string NameElementName = "name";
        private const string SurnameElementName = "surname";
        private const string PhoneNumberElementName = "phoneNumber";
        private const string EmailAddressElementName = "emailAddress";

        public string FromContent(ContactBook content)
        {
            var xml = new XDocument();
            var root = new XElement("contactBook");
            xml.Add(root);
            foreach (var contact in content.Contacts)
            {
                var contactElement = new XElement(ContactElementName);
                contactElement.Add(
                    new XElement(NameElementName, contact.Name),
                    new XElement(SurnameElementName, contact.Surname),
                    new XElement(PhoneNumberElementName, contact.PhoneNumber),
                    new XElement(EmailAddressElementName, contact.EmailAddress));
                root.Add(contactElement);
            }

            return xml.ToString();
        }

        public ContactBook ToContent(string readContent)
        {
            var contactBook = new ContactBook();
            var xml = XDocument.Parse(readContent);
            var contacts = xml.Root.Elements(ContactElementName);
            foreach (var contact in contacts)
                contactBook.Add(ParseContact(contact));

            return contactBook;
        }

        private Contact ParseContact(XElement element)
        {
            return new Contact(
                element.Element(NameElementName).Value,
                element.Element(SurnameElementName).Value,
                element.Element(PhoneNumberElementName).Value,
                element.Element(EmailAddressElementName).Value
                );
        }
    }
}
