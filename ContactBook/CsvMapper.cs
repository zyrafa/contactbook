﻿using System;
using System.Text;

namespace ContactBook
{
    public class CsvMapper : IContentMapper<ContactBook>
    {
        public string FromContent(ContactBook content)
        {
            var csv = new StringBuilder();

            foreach (var contact in content.Contacts)
                csv.AppendLine($"{contact.Surname},{contact.Name},{contact.PhoneNumber},{contact.EmailAddress}");

            return csv.ToString();
        }

        public ContactBook ToContent(string readContent)
        {
            var contactBook = new ContactBook();
            var readLines = readContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in readLines)
                contactBook.Add(ParseContact(line));

            return contactBook;
        }

        private Contact ParseContact(string line)
        {
            var splittedLine = line.Split(',');
            return new Contact(
                splittedLine[0],
                splittedLine[1],
                splittedLine[2],
                splittedLine[3]);
        }
    }
}
