﻿namespace ContactBook
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var contactBook = new ContactBook();
            contactBook.Add(new Contact("M", "P", "999999999", "m@e.pl"));
            contactBook.Add(new Contact("P", "R", "999999999", "m@e.pl"));
            contactBook.Add(new Contact("Ł", "F", "999999999", "m@e.pl"));

            System.Console.WriteLine("Original contact book:");
            contactBook.Print();

            const string filepath = @"C:\users\student12\desktop\rr.csv";

            var fileRepository = new FileRepository<ContactBook>(new JsonMapper());
            fileRepository.Save(filepath, contactBook);

            var contactBookFromFile = fileRepository.Read(filepath);
            System.Console.WriteLine("\nContact book from file:");
            contactBookFromFile.Print();
        }
    }
}
