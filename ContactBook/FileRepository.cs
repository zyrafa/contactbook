﻿using System.IO;

namespace ContactBook
{
    public class FileRepository<T>
    {
        private readonly IContentMapper<T> contentMapper;

        public FileRepository(IContentMapper<T> contentMapper)
        {
            this.contentMapper = contentMapper;
        }

        public void Save(string filePath, T content)
        {
            File.WriteAllText(filePath, contentMapper.FromContent(content));
        }

        public T Read(string filepath)
        {
            var readContent = File.ReadAllText(filepath);
            return contentMapper.ToContent(readContent);
        }
    }
}
