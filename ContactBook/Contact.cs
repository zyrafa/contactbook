﻿namespace ContactBook
{
    public class Contact
    {
        public string Name { get; set; } 
        public string Surname { get; set; }
        public string PhoneNumber{ get; set; }
        public string EmailAddress { get; set; }
        
        public Contact(string name, string surName, string phoneNumber, string emailAddress)
        {
            Name = name;
            Surname = surName;
            PhoneNumber = phoneNumber;
            EmailAddress = emailAddress;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = obj as Contact;
            return other.Name == Name && other.Surname == Surname;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
