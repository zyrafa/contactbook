﻿using System.Collections.Generic;
using System.Linq;

namespace ContactBook
{
    public class ContactBook
    {
        public readonly List<Contact> contacts;

        public ContactBook()
        {
            contacts = new List<Contact>();
        }

        public ContactBook(IEnumerable<Contact> contacts)
        {
            this.contacts = contacts.ToList();
        }

        public void Add(Contact contact)
        {
            if (!contacts.Contains(contact))
            {
                contacts.Add(contact);
            }
        }

        public void Print()
        {
            var orderedContacts = contacts.OrderBy(c => c.Surname);
            foreach (var contact in orderedContacts)
                System.Console.WriteLine($"{contact.Surname} {contact.Name}: {contact.PhoneNumber}");
        }

        public IReadOnlyCollection<Contact> Contacts => contacts.AsReadOnly();
    }
}
