﻿using Newtonsoft.Json;

namespace ContactBook
{
    public class JsonMapper : IContentMapper<ContactBook>
    {
        public string FromContent(ContactBook content)
        {
            return JsonConvert.SerializeObject(content, Formatting.Indented);
        }

        public ContactBook ToContent(string readContent)
        {
            return JsonConvert.DeserializeObject<ContactBook>(readContent);
        }
    }
}
