﻿namespace ContactBook
{
    public interface IContentMapper<T>
    {
        string FromContent(T content);
        T ToContent(string readContent);
    }
}
